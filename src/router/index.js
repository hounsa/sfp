import Employe from "../pages/Employe";
import Filiale from "../pages/Filiale";
import User from "../pages/Employe/Info";
import EditUser from "../pages/Employe/Edit";
import Auth from "../pages/Auth";

const routes = [
  {
    path: "/",
    name: "home",
    component: Employe,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/employes",
    name: "employes",
    component: Employe,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/filiales",
    name: "filiales",
    component: Filiale,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/employe/:userId",
    name: "userDetails",
    component: User,
    props: true,
  },
  {
    path: "/employe/edition/:userId",
    name: "editUser",
    component: EditUser,
    props: true,
  },
  {
    path: "/employe/nouveau",
    name: "newUser",
    component: EditUser,
    props: false,
  },
  {
    path: "/connexion",
    name: "authentification",
    component: Auth,
  },
];

export default routes;
