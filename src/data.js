const users = [
  {
    id: 1,
    nom: "Hounsa",
    prenom: "Yvon",
    cniNumber: "20136848965",
    dateNaissance: "22/12/1992",
    nationalite: "Béninoise",
    lieuNaissance: "Cotonou",
    langues: "Goun, fon",

    filiale: {
      nom: "Atral SA",
      adresse: "Cotonou, Cadjehoun",
      telephone: "21 33 02 37",
    },
    enfants: [
      {
        _id: 1,
        nom: "Hounsa",
        prenom: "Moise",
        dateNaissance: "22/12/192",
      },
      {
        _id: 2,
        nom: "Hounsa",
        prenom: "Moise",
        dateNaissance: "22/12/192",
      },
      {
        _id: 3,
        nom: "Hounsa",
        prenom: "Moise",
        dateNaissance: "22/12/192",
      },
    ],
  },
  {
    id: 2,
    nom: "Hounsa",
    prenom: "Yvon",
    cniNumber: "20136848965",
    dateNaissance: "22/12/1992",
    nationalite: "Béninoise",
    lieuNaissance: "Cotonou",
    langues: "Goun, fon",

    enfants: [
      {
        _id: 1,
        nom: "Hounsa",
        prenom: "Moise",
        dateNaissance: "22/12/192",
      },
      {
        _id: 2,
        nom: "Hounsa",
        prenom: "Moise",
        dateNaissance: "22/12/192",
      },
      {
        _id: 3,
        nom: "Hounsa",
        prenom: "Moise",
        dateNaissance: "22/12/192",
      },
    ],
  },
  {
    id: 3,
    nom: "Hounsa",
    prenom: "Yvon",
    cniNumber: "20136848965",
    dateNaissance: "22/12/1992",
    nationalite: "Béninoise",
    lieuNaissance: "Cotonou",
    langues: "Goun, fon",

    enfants: [
      {
        _id: 1,
        nom: "Hounsa",
        prenom: "Moise",
        dateNaissance: "22/12/192",
      },
      {
        _id: 2,
        nom: "Hounsa",
        prenom: "Moise",
        dateNaissance: "22/12/192",
      },
      {
        _id: 3,
        nom: "Hounsa",
        prenom: "Moise",
        dateNaissance: "22/12/192",
      },
    ],
  },
];

const initialFilialeValue = {
  nom: "",
  adresse: "",
  telephone: "",
};

const initialEnfantValue = {
  nom: "",
  prenom: "",
  dateNaissance: "",
};

const initialUserValue = {
  nom: "Amoussu",
  prenom: "",
  cniNumber: "",
  dateNaissance: "",
  nationalite: "",
  lieuNaissance: "",
  langues: ["Goun"],

  filiale: undefined,
  enfants: [],
};

const getUser = () => {
  return users[0];
};

const getFiliale = () => {
  return [
    {
      id: 1,
      nom: "Atral",
      adresse: "Cadjehoun",
      telephone: "21 37 38 40",
    },
  ];
};

export {
  users,
  getUser,
  initialUserValue,
  initialFilialeValue,
  initialEnfantValue,
  getFiliale,
};
