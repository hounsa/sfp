import reqwest from "reqwest";

const fakeDataUrl =
  "https://randomuser.me/api/?results=5&inc=name,gender,email,nat&noinfo";

const getUserListApi = (callback) => {
  reqwest({
    url: fakeDataUrl,
    type: "json",
    method: "get",
    contentType: "application/json",
    success: (res) => {
      console.log({ res });
      callback(res);
    },
  });
};

export { getUserListApi };
