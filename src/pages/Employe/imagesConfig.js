import user1 from "../../assets/user1.png";
import user2 from "../../assets/user2.png";
import user3 from "../../assets/user3.png";
import user4 from "../../assets/user4.png";
import user5 from "../../assets/user5.png";
import user6 from "../../assets/user6.png";
import user7 from "../../assets/user7.png";
import user8 from "../../assets/user8.png";
import user9 from "../../assets/user9.png";
import user10 from "../../assets/user10.png";
import user11 from "../../assets/user11.png";
import user12 from "../../assets/user12.png";
import user13 from "../../assets/user13.png";
import user14 from "../../assets/user14.png";
import user15 from "../../assets/user15.png";

const images = [
  user1,
  user2,
  user3,
  user4,
  user5,
  user6,
  user7,
  user8,
  user9,
  user10,
  user11,
  user12,
  user13,
  user14,
  user15,
];

function getImageRAndom() {
  const index = Math.floor(Math.random() * 10);
  return images[index];
}

export { getImageRAndom };
