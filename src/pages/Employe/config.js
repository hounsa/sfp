const defaultData = {
  nom: "",
  prenom: "",
  dateNaissance: undefined,
  lieuNaissance: "",
  nationalite: "",
  langParle: "",
  cniNumber: "",
  cnssNumber: "",
  enfants: [],
  langues: [],
};

const defaultEnfantData = {
  nom: "",
  prenom: "",
  dateNaissance: undefined,
};
export { defaultData, defaultEnfantData };
