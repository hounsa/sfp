const showSuccessNote = async (ref, loadingMessage, successMessage) =>
  await ref.$message
    .loading(loadingMessage, 1.5)
    .then(() => ref.$message.success(successMessage, 1.5));

const showErrorNote = (ref, loadingMessage, errorMessage, temps=1.5) =>
  ref.$message
    .loading(loadingMessage, 1.5)
    .then(() => ref.$message.error(errorMessage, temps));

export { showSuccessNote, showErrorNote };
