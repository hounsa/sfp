import Vue from "vue";
import Vuex from "vuex";

import App from "./App.vue";
import routes from "./router";

import store from "./store";
import axios from "axios";

import Antd from "ant-design-vue";
import VueRouter from "vue-router";
import Fragment from "vue-fragment";
import VueMoment from "vue-moment";
import { BACKEND_URL } from "../api";

import "ant-design-vue/dist/antd.css";

axios.defaults.baseURL = BACKEND_URL;

Vue.config.productionTip = false;

Vue.use(Vuex);
Vue.use(Antd);
Vue.use(VueRouter);
Vue.use(Fragment.Plugin);
Vue.use(VueMoment);

Vue.prototype.$http = axios;
const token = localStorage.getItem("token");
if (token) {
  Vue.prototype.$http.defaults.headers.common["Authorization"] = token;
}

const router = new VueRouter({
  routes, // short for `routes: routes`
  mode: "history",
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (store.getters.isAuthenticated) {
      next();
      return;
    }
    next("/connexion");
  } else {
    next();
  }
});

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
