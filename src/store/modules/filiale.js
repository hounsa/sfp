//store/modules/auth.js

import axios from "axios";

const state = {
  filiales: [],
};

const getters = {
  StateFiliales: (state) => state.filiales,
};

const mutations = {
  setFiliales(state, value) {
    state.filiales = [...value];
  },

  pushFiliale(state, value) {
    state.filiales.push({ ...value });
  },
};

const actions = {
  //

  async UpsertFiliale({ dispatch, state, getters }, body) {
    return new Promise((resolve, reject) => {
      const userId = state.user._id;

      const isUpdate = body._id !== undefined;

      const fetchREquest = () =>
        !isUpdate
          ? axios.post(`filiale/create/${userId}`, body, getters.getHeader)
          : axios.put(`filiale/${body._id}/${userId}`, body, getters.getHeader);

      fetchREquest()
        .then(async (result) => {
          const filiale = result.data.filiale;
          await dispatch("GetFiliales");
          resolve(filiale);
        })
        .catch((err) => {
          reject(err.response.data.error);
        });
    });
  },

  async GetFiliales({ commit, state, getters }) {
    return new Promise((resolve, reject) => {
      const userId = state.user._id;
      axios
        .get("filiales/" + userId, getters.getHeader)
        .then(async (result) => {
          await commit("setFiliales", result.data);
          resolve(result.data);
        })
        .catch((err) => {
          console.log({ err });
          reject();
        });
    });
  },

  async GetFiliale({ state, getters }, id) {
    return new Promise((resolve, reject) => {
      const userId = state.user._id;
      axios
        .get(`filiale/${id}/${userId}`, getters.getHeader)
        .then(async (result) => {
          resolve(result.data);
        })
        .catch((err) => {
          let message = err.response.data.error;
          reject(message);
        });
    });
  },

  async RemoveFiliale({ state, getters, dispatch }, id) {
    return new Promise((resolve, reject) => {
      const userId = state.user._id;
      axios
        .delete(`filiale/${id}/${userId}`, getters.getHeader)
        .then(async () => {
          resolve();
          await dispatch("GetFiliales");
        })
        .catch((err) => {
          let message = err.response.data.error;
          reject(message);
        });
    });
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
