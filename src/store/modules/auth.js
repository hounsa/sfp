//store/modules/auth.js

import filialeModule from "./filiale";
import employeModule from "./employe";

import axios from "axios";
const state = {
  user: null,
  token: "",
  broken: false,
  ...filialeModule.state,
  ...employeModule.state,
};

const getters = {
  isAuthenticated: (state) => !!state.user,
  StateUser: (state) => state.user,
  isBroken: (state) => state.broken,
  getHeader: (state) => ({
    headers: { Authorization: `Bearer ${state.token}` },
  }),
  ...filialeModule.getters,
  ...employeModule.getters,
};

const mutations = {
  setUser(state, user) {
    state.user = user;
  },

  setToken(state, token) {
    state.token = token;
  },

  changeBroken(state, broken) {
    state.broken = broken;
  },

  logOut(state) {
    state.user = null;
    state.employes = null;
    state.token = "";
  },
  ...filialeModule.mutations,
  ...employeModule.mutations,
};

const actions = {
  //

  async Register({ dispatch }, userBody) {
    await axios
      .post("signin", userBody)
      .then(async () => {
        let UserForm = new FormData();
        UserForm.append("username", userBody.username);
        UserForm.append("email", userBody.email);
        UserForm.append("password", userBody.password);
        await dispatch("LogIn", UserForm);
      })
      .catch((error) => {
        console.log({ error });
      });
  },

  setBroken({ commit }, brok) {
    commit("changeBroken", brok);
  },

  async LogIn({ commit }, body) {
    return new Promise((resolve, reject) => {
      axios
        .post("signin", body)
        .then(async (result) => {
          const user = result.data.user;
          const token = result.data.token;
          await commit("setUser", user);
          await commit("setToken", token);
          resolve();
        })
        .catch((err) => {
          reject(err.response.data.error);
        });
    });
  },

  async LogOut({ commit }) {
    let user = null;
    commit("logOut", user);
  },

  ...filialeModule.actions,
  ...employeModule.actions,
};

export default {
  state,
  getters,
  actions,
  mutations,
};
