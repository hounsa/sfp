//store/modules/auth.js

import axios from "axios";

const state = {
  employes: [],
};

const getters = {
  StateEmployes: (state) => state.employes,
};

const mutations = {
  setEmployes(state, value) {
    state.employes = [...value];
  },

  pushEmploye(state, value) {
    state.employes.push({ ...value });
  },
};

const actions = {
  //

  async UpsertEmploye({ dispatch, state, getters }, body) {
    return new Promise((resolve, reject) => {
      const userId = state.user._id;

      const isUpdate = body._id !== undefined;

      const fetchREquest = () =>
        !isUpdate
          ? axios.post(`employe/create/${userId}`, body, getters.getHeader)
          : axios.put(`employe/${body._id}/${userId}`, body, getters.getHeader);

      fetchREquest()
        .then(async (result) => {
          const employe = result.data.employe;
          await dispatch("GetEmployes");
          resolve(employe);
        })
        .catch((err) => {
          reject(err.response.data.error);
        });
    });
  },

  async GetEmployes({ commit, state, getters }) {
    return new Promise((resolve, reject) => {
      const userId = state.user._id;
      axios
        .get("employes/" + userId, getters.getHeader)
        .then(async (result) => {
          await commit("setEmployes", result.data);
          resolve();
        })
        .catch((err) => {
          console.log({ err });
          reject();
        });
    });
  },

  async GetEmploye({ state, getters }, id) {
    return new Promise((resolve, reject) => {
      const userId = state.user._id;

      axios
        .get(`employe/${id}/${userId}`, getters.getHeader)
        .then(async (result) => {
          resolve(result.data);
        })
        .catch((err) => {
          let message = err.response.data.error;
          reject(message);
        });
    });
  },

  async RemoveEmploye({ state, getters, dispatch }, id) {
    return new Promise((resolve, reject) => {
      const userId = state.user._id;
      axios
        .delete(`employe/${id}/${userId}`, getters.getHeader)
        .then(async () => {
          resolve();
          await dispatch("GetEmployes");
        })
        .catch((err) => {
          let message = err.response.data.error;
          reject(message);
        });
    });
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
