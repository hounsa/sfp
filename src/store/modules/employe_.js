//store/modules/auth.js

import axios from "axios";
const state = {
  employes: [],
};

const getters = {
  StateEmployes: (state) => state.employes,
};

const mutations = {
  setEmployes(state, employes) {
    state.employes = employes;
  },

  pushEmploye(state, employe) {
    state.employes.push(employe);
  },
};

const actions = {
  //

  async CreateEmploye({ commit, rootState }, body) {
    return new Promise((resolve, reject) => {
      const userId = rootState.user._id;
      console.log({ rootState, userId: rootState.user._id });

      axios
        .post("employe/create/" + userId, body)
        .then(async (result) => {
          const employe = result.data.employe;
          await commit("pushEmploye", employe);

          // await dispatch("GetEmployes");
          resolve();
        })
        .catch((err) => {
          reject(err.response.data.error);
        });
    });
  },

  async GetEmployes({ commit, state, rootGetters }) {
    return new Promise((resolve, reject) => {
      const userId = state.user._id;
      axios
        .get("employes/" + userId, rootGetters.getHeader)
        .then(async (result) => {
          await commit("setEmployes", result.data);
          resolve();
        })
        .catch((err) => {
          console.log({ err });
          reject();
        });
    });
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
