# SFP

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### BACKEND API SETING

SET UP BACKEND API URL IN ./api

### USER NAME AND PASSWORD FOR AUTHENTIFICATION

username : demo@sfp.com
password : 123456
